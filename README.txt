Views Exposed multilingual taxonomy filter

This module allows displaying of taxonomy term reference exposed filters with
the language option (if there is one) in the label like: "term_name (language)".

The module provides a setting to taxonomy_term_reference views filter.
You have juste to check to box for each views filter you want to have this
behaviour.

Getting the language is done with entity_language() core function.