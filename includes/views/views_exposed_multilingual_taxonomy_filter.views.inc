<?php

/**
 * @file
 * Provide views data and handlers for views_exposed_multilingual_taxonomy_filter.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data_alter().
 */
function views_exposed_multilingual_taxonomy_filter_views_data_alter(&$data) {
  $default_handler = 'views_handler_filter_term_node_tid';
  $new_handler = 'views_exposed_multilingual_taxonomy_filter_term_node_tid';
  // Replace filter handler with our handler.
  foreach ($data as $table_name => $table_info) {
    foreach ($table_info as $field_name => $field_info) {
      if (!empty($field_info['filter']['handler']) && $field_info['filter']['handler'] == $default_handler) {
        $data[$table_name][$field_name]['filter']['handler'] = $new_handler;
      }
    }
  }
}
