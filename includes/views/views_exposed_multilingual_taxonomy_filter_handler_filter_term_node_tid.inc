<?php

/**
 * @file
 * Contains views_exposed_multilingual_taxonomy_filter_handler_filter_term_node_tid.
 */

/**
 * Provides a views filter handler for exposed taxonomy filters.
 * @see views_handler_filter_term_node_tid
 */
class views_exposed_multilingual_taxonomy_filter_term_node_tid extends views_handler_filter_term_node_tid {

  /**
   * {@inheritdoc}
   */
  function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);

    $form['type']['#options'] += array(
      'display_language_in_label' => t('Display the term language in the label.'),
    );
    // Add dependency hierarchy option.
    $hierarchy_dependency = &$form['hierarchy']['#dependency']['radio:options[type]'];
    array_push($hierarchy_dependency, 'display_language_in_label');
  }

  /**
   * {@inheritdoc}
   */
  function admin_summary() {
    // Single values should be wrapped in an array.
    if (is_string($this->value)) {
      $this->value = array($this->value);
    }
    return parent::admin_summary();
  }

  /**
   * {@inheritdoc}
   */
  function value_form(&$form, &$form_state) {
    $vocabulary = taxonomy_vocabulary_machine_name_load($this->options['vocabulary']);
    if (empty($vocabulary) && $this->options['limit']) {
      $form['markup'] = array(
        '#markup' => '<div class="form-item">' . t('An invalid vocabulary is selected. Please change it in the options.') . '</div>',
      );
      return;
    }

    if ($this->options['type'] == 'textfield') {
      $default = '';
      if ($this->value) {
        $result = taxonomy_term_load_multiple($this->value);
        foreach ($result as $entity_term) {
          if ($default) {
            $default .= ', ';
          }
          $default .= entity_label('taxonomy_term', $entity_term);
          if ($this->options['type'] == 'display_language_in_label') {
            $default .= ' (' . entity_language('taxonomy_term', $entity_term) . ')';
          }
        }
      }

      $form['value'] = array(
        '#title' => $this->options['limit'] ? t('Select terms from vocabulary @voc', array('@voc' => $vocabulary->name)) : t('Select terms'),
        '#type' => 'textfield',
        '#default_value' => $default,
      );

      if ($this->options['limit']) {
        $form['value']['#autocomplete_path'] = 'admin/views/ajax/autocomplete/taxonomy/' . $vocabulary->vid;
      }
    }
    else {
      if (!empty($this->options['hierarchy']) && $this->options['limit']) {
        $tree = taxonomy_get_tree($vocabulary->vid, 0, NULL, TRUE);
        $options = array();

        if ($tree) {
          // Translation system needs full entity objects, so we have access to label.
          foreach ($tree as $term) {
            $choice = new stdClass();
            $label = str_repeat('-', $term->depth) . entity_label('taxonomy_term', $term);
            if ($this->options['type'] == 'display_language_in_label') {
              $label.= ' (' . entity_language('taxonomy_term', $term) . ')';
            }
            $choice->option = array($term->tid => $label);
            $options[] = $choice;
          }
        }
      }
      else {
        $options = array();
        $query = db_select('taxonomy_term_data', 'td');
        $query->innerJoin('taxonomy_vocabulary', 'tv', 'td.vid = tv.vid');
        $query->fields('td');
        $query->orderby('tv.weight');
        $query->orderby('tv.name');
        $query->orderby('td.weight');
        $query->orderby('td.name');
        $query->addTag('term_access');
        if ($this->options['limit']) {
          $query->condition('tv.machine_name', $vocabulary->machine_name);
        }
        $result = $query->execute();

        $tids = array();
        foreach ($result as $term) {
          $tids[] = $term->tid;
        }
        $entities = taxonomy_term_load_multiple($tids);
        foreach ($entities as $entity_term) {
          $options[$entity_term->tid] = entity_label('taxonomy_term', $entity_term);
          if ($this->options['type'] == 'display_language_in_label') {
            $options[$entity_term->tid] .= ' (' . entity_language('taxonomy_term', $entity_term) . ')';
          }
        }
      }

      $default_value = (array) $this->value;

      if (!empty($form_state['exposed'])) {
        $identifier = $this->options['expose']['identifier'];

        if (!empty($this->options['expose']['reduce'])) {
          $options = $this->reduce_value_options($options);

          if (!empty($this->options['expose']['multiple']) && empty($this->options['expose']['required'])) {
            $default_value = array();
          }
        }

        if (empty($this->options['expose']['multiple'])) {
          if (empty($this->options['expose']['required']) && (empty($default_value) || !empty($this->options['expose']['reduce']))) {
            $default_value = 'All';
          }
          elseif (empty($default_value)) {
            $keys = array_keys($options);
            $default_value = array_shift($keys);
          }
          // Due to #1464174 there is a chance that array('') was saved in the admin ui.
          // Let's choose a safe default value.
          elseif ($default_value == array('')) {
            $default_value = 'All';
          }
          else {
            $copy = $default_value;
            $default_value = array_shift($copy);
          }
        }
      }
      $form['value'] = array(
        '#type' => 'select',
        '#title' => $this->options['limit'] ? t('Select terms from vocabulary @voc', array('@voc' => $vocabulary->name)) : t('Select terms'),
        '#multiple' => TRUE,
        '#options' => $options,
        '#size' => min(9, count($options)),
        '#default_value' => $default_value,
      );

      if (!empty($form_state['exposed']) && isset($identifier) && !isset($form_state['input'][$identifier])) {
        $form_state['input'][$identifier] = $default_value;
      }
    }


    if (empty($form_state['exposed'])) {
      // Retain the helper option
      $this->helper->options_form($form, $form_state);
    }
  }

}
